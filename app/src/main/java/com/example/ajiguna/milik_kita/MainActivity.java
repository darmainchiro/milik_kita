package com.example.ajiguna.milik_kita;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.example.ajiguna.milik_kita.modules.account.AccountFragment;
import com.example.ajiguna.milik_kita.modules.history.HistoryFragment;
import com.example.ajiguna.milik_kita.modules.home.HomeFragment;

public class MainActivity extends AppCompatActivity {

    private String mPost_key;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        toolbar = (Toolbar)findViewById(R.id.nav_action);
        toolbar.setTitle("MilikKita");
        mPost_key = getIntent().getExtras().getString("id_user");

        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        Fragment selectedFragment = new HomeFragment();
        Bundle data = new Bundle();//Use bundle to pass data
        data.putString("data", mPost_key);//put string, int, etc in bundle with a key value
        selectedFragment.setArguments(data);

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_layout, selectedFragment);
        transaction.commit();
    }

    BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {

            Fragment selectedFragment = null;
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    selectedFragment = new HomeFragment();
                    Bundle data_post = new Bundle();//Use bundle to pass data
                    data_post.putString("data", mPost_key);//put string, int, etc in bundle with a key value
                    selectedFragment.setArguments(data_post);
                    break;
                case R.id.navigation_history:
                    selectedFragment = new HistoryFragment();
                    Bundle data_history = new Bundle();//Use bundle to pass data
                    data_history.putString("data", mPost_key);//put string, int, etc in bundle with a key value
                    selectedFragment.setArguments(data_history);
                    break;
                case R.id.navigation_account:
                    selectedFragment = new AccountFragment();//Get Fragment Instance
                    Bundle data = new Bundle();//Use bundle to pass data
                    data.putString("data", mPost_key);//put string, int, etc in bundle with a key value
                    selectedFragment.setArguments(data);
                    break;
            }
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.frame_layout, selectedFragment);
            transaction.commit();
            return true;
        }
    };


}
