package com.example.ajiguna.milik_kita.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.ajiguna.milik_kita.R;
import com.example.ajiguna.milik_kita.model.Latest_Post;
import com.example.ajiguna.milik_kita.modules.article.ArticleActivity;

import java.util.ArrayList;
import java.util.List;


public class DonasiAdapter extends RecyclerView.Adapter<DonasiAdapter.MyViewHolder> {

    private List<Latest_Post> postku;
    ArrayList<String> personNames;
    ArrayList<Integer> personImages;
    String user;
    Context context;
    int index;

    public DonasiAdapter(Context context,  List<Latest_Post> postku, String user) {
        this.context = context;
        this.postku = postku;
        this.user = user;
    }



    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // infalte the item Layout
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_donasi, parent, false);
        // set the view's size, margins, paddings and layout parameters
        MyViewHolder vh = new MyViewHolder(v); // pass the view to View Holder
        return vh;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        // set the data in items
        Double nilai = Double.valueOf(postku.get(position).getDonasiTerkumpul()).doubleValue() * 100 / Double.valueOf(postku.get(position).getDonasiButuh()).doubleValue();
        String pencapaian = String.format("%.2f", nilai);
        holder.judul.setText(postku.get(position).getNamaWisata());
        holder.nilai_dana.setText("Rp " + postku.get(position).getDonasiButuh());
        holder.nilai_capai.setText(pencapaian +"%");
        holder.sisa_hari.setText(postku.get(position).getSisaHari());
        Glide.with(context).load(postku.get(position).getGambar()).into(holder.image);

        // implement setOnClickListener event on item view.
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(context, ArticleActivity.class);
                i.putExtra("id_wisata", postku.get(position).getIdWisata());
                i.putExtra("id_user",user);
                context.startActivity(i);
            }
        });

    }


    @Override
    public int getItemCount() {
        return postku == null ? 0 : postku.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        // init the item view's
        TextView judul;
        TextView nilai_dana,nilai_capai,sisa_hari;
        ImageView image;
        ProgressBar progres_donasi;
        View nView;
        Button buttonku;

        public MyViewHolder(View itemView) {
            super(itemView);
            nView = itemView;

            // get the reference of item view's
            judul = (TextView) itemView.findViewById(R.id.teks_judul);
            image = (ImageView) itemView.findViewById(R.id.gambar);
            nilai_dana = (TextView)itemView.findViewById(R.id.nilai_dana);
            nilai_capai = (TextView)itemView.findViewById(R.id.nilai_pencapaian);
            sisa_hari = (TextView)itemView.findViewById(R.id.sisa_hari);
            progres_donasi = (ProgressBar)itemView.findViewById(R.id.progres_donasi);

            progres_donasi.getResources().getColor(R.color.colorPrimary);
            progres_donasi.setProgress(23);

            // buttonku = (Button)itemView.findViewById(R.id.postbutton);
//            nView.setOnClickListener(this);
        }


        @Override
        public void onClick(View v) {

            Context context = v.getContext();
            final Intent intent;
            switch (getAdapterPosition()){
                case 0:

                    intent =  new Intent(context, ArticleActivity.class);
                    intent.putExtra("id_post", personNames);
                    break;
                case 1:
                    intent =  new Intent(context, ArticleActivity.class);
                    break;

                case 2:
                    intent =  new Intent(context, ArticleActivity.class);
                    break;

                default:
                    intent =  new Intent(context, ArticleActivity.class);
                    break;
            }
            context.startActivity(intent);

        }
    }



}
