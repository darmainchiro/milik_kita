package com.example.ajiguna.milik_kita.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.ajiguna.milik_kita.R;
import com.example.ajiguna.milik_kita.model.Detail_Post_Donatur;

import java.util.ArrayList;
import java.util.List;


public class DonaturAdapter extends RecyclerView.Adapter<DonaturAdapter.MyViewHolder> {

    ArrayList<String> personNames;
    ArrayList<String> uang_donasi;
    private List<Detail_Post_Donatur> postku;
    Context context;
    int index;

    public DonaturAdapter(Context context, List<Detail_Post_Donatur> postku) {
        this.context = context;
        this.postku = postku;
    }



    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // infalte the item Layout
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_donatur, parent, false);
        // set the view's size, margins, paddings and layout parameters
        MyViewHolder vh = new MyViewHolder(v); // pass the view to View Holder
        return vh;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        // set the data in items
        holder.name_donatur.setText(postku.get(position).getUserName());
        holder.uang_donasi.setText("Rp " + postku.get(position).getJumDonasi());
        holder.date_donatur.setText(postku.get(position).getTglDonasi());
        holder.komentar.setText(postku.get(position).getKomentar());
    }


    @Override
    public int getItemCount() {
        return postku == null ? 0 : postku.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        // init the item view's
        TextView name_donatur;
        TextView uang_donasi;
        TextView date_donatur;
        TextView komentar;
        View nView;

        public MyViewHolder(View itemView) {
            super(itemView);
            nView = itemView;

            // get the reference of item view's
            name_donatur = (TextView) itemView.findViewById(R.id.name_donatur);
            uang_donasi = (TextView) itemView.findViewById(R.id.uang_donasi);
            date_donatur = (TextView) itemView.findViewById(R.id.date_donatur);
            komentar = (TextView)itemView.findViewById(R.id.komentar);

            // buttonku = (Button)itemView.findViewById(R.id.postbutton);
//            nView.setOnClickListener(this);
        }

    }



}
