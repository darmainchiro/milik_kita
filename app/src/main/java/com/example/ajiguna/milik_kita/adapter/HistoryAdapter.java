package com.example.ajiguna.milik_kita.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.ajiguna.milik_kita.R;
import com.example.ajiguna.milik_kita.model.History;

import java.util.List;


public class HistoryAdapter extends RecyclerView.Adapter<HistoryAdapter.MyViewHolder> {

    private List<History> postku;
    Context context;
    int index;

    public HistoryAdapter(Context context, List<History> postku) {
        this.context = context;
        this.postku = postku;
    }



    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // infalte the item Layout
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_history, parent, false);
        // set the view's size, margins, paddings and layout parameters
        MyViewHolder vh = new MyViewHolder(v); // pass the view to View Holder
        return vh;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        // set the data in items
        holder.name_wisata.setText(postku.get(position).getNamaWisata());
        holder.jumlah_donasi.setText("Rp " + postku.get(position).getJumDonasi());
        holder.date_donasi.setText(postku.get(position).getTglDonasi());
    }


    @Override
    public int getItemCount() {
        return postku == null ? 0 : postku.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        // init the item view's
        TextView name_wisata;
        TextView jumlah_donasi;
        TextView date_donasi;
        View nView;

        public MyViewHolder(View itemView) {
            super(itemView);
            nView = itemView;

            // get the reference of item view's
            name_wisata = (TextView) itemView.findViewById(R.id.nama_wisata);
            jumlah_donasi = (TextView) itemView.findViewById(R.id.besar_donasi);
            date_donasi = (TextView) itemView.findViewById(R.id.date_history);
//            image = (ImageView) itemView.findViewById(R.id.gambar);

            // buttonku = (Button)itemView.findViewById(R.id.postbutton);
//            nView.setOnClickListener(this);
        }

    }



}
