package com.example.ajiguna.milik_kita.api;


import com.example.ajiguna.milik_kita.model.Detail_Post;
import com.example.ajiguna.milik_kita.model.Detail_Post_Donatur;
import com.example.ajiguna.milik_kita.model.Detail_Post_Story;
import com.example.ajiguna.milik_kita.model.Donasi;
import com.example.ajiguna.milik_kita.model.Galang;
import com.example.ajiguna.milik_kita.model.History;
import com.example.ajiguna.milik_kita.model.History_Campaign;
import com.example.ajiguna.milik_kita.model.Latest_Post;
import com.example.ajiguna.milik_kita.model.Login;
import com.example.ajiguna.milik_kita.model.Profile;
import com.example.ajiguna.milik_kita.model.Register;

import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by Aji Guna on 28/07/2017.
 */

public interface ApiInterface {

    @FormUrlEncoded
    @POST("login.php")
    Call<Login> getLogin(
            @FieldMap HashMap<String, String> params
    );

    @FormUrlEncoded
    @POST("register.php")
    Call<Register> getRegister(
            @FieldMap HashMap<String, String> params
    );

    @FormUrlEncoded
    @POST("donasi/input_donasi.php")
    Call<Donasi> getDonasi(
            @FieldMap HashMap<String, String> params
    );

    @FormUrlEncoded
    @POST("wisata/input_wisata.php")
    Call<Galang> getGalang(
            @FieldMap HashMap<String, String> params
    );

    @GET("wisata/get_wisata.php")
    Call <List<Latest_Post>> getPost();

    @GET("wisata/get_wisata_detail.php")
    Call<List<Detail_Post>> getPostDetail(@Query("id_wisata") String id_wisata);

    @GET("wisata/get_wisata_story.php")
    Call<List<Detail_Post_Story>> getPostDetailStory(@Query("id_wisata") String id_wisata);

    @GET("wisata/get_wisata_donatur.php")
    Call<List<Detail_Post_Donatur>> getPostDetailDonatur(@Query("id_wisata") String id_wisata);

    @GET("user/get_user_profile.php")
    Call<List<Profile>> getProfile(@Query("id_user") String id_user);

    @GET("user/get_user_history.php")
    Call <List<History>> getHistory(@Query("id_user") String id_user);

    @GET("wisata/get_wisata_galang.php")
    Call <List<History_Campaign>> getCampaign(@Query("id_user") String id_user);

}