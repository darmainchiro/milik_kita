package com.example.ajiguna.milik_kita.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Aji Guna on 18/04/2018.
 */

public class Data_Login {

    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("jum_donasi")
    @Expose
    private String jumDonasi;
    @SerializedName("jum_transaksi")
    @Expose
    private String jumTransaksi;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getJumDonasi() {
        return jumDonasi;
    }

    public void setJumDonasi(String jumDonasi) {
        this.jumDonasi = jumDonasi;
    }

    public String getJumTransaksi() {
        return jumTransaksi;
    }

    public void setJumTransaksi(String jumTransaksi) {
        this.jumTransaksi = jumTransaksi;
    }
}
