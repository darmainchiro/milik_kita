package com.example.ajiguna.milik_kita.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Aji Guna on 20/04/2018.
 */

public class Detail_Post_Donatur {

    @SerializedName("id_donasi")
    @Expose
    private String idDonasi;
    @SerializedName("id_wisata")
    @Expose
    private String idWisata;
    @SerializedName("nama_wisata")
    @Expose
    private String namaWisata;
    @SerializedName("id_user")
    @Expose
    private String idUser;
    @SerializedName("user_name")
    @Expose
    private String userName;
    @SerializedName("jum_donasi")
    @Expose
    private String jumDonasi;
    @SerializedName("tgl_donasi")
    @Expose
    private String tglDonasi;
    @SerializedName("komentar")
    @Expose
    private String komentar;

    public String getIdDonasi() {
        return idDonasi;
    }

    public void setIdDonasi(String idDonasi) {
        this.idDonasi = idDonasi;
    }

    public String getIdWisata() {
        return idWisata;
    }

    public void setIdWisata(String idWisata) {
        this.idWisata = idWisata;
    }

    public String getNamaWisata() {
        return namaWisata;
    }

    public void setNamaWisata(String namaWisata) {
        this.namaWisata = namaWisata;
    }

    public String getIdUser() {
        return idUser;
    }

    public void setIdUser(String idUser) {
        this.idUser = idUser;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getJumDonasi() {
        return jumDonasi;
    }

    public void setJumDonasi(String jumDonasi) {
        this.jumDonasi = jumDonasi;
    }

    public String getTglDonasi() {
        return tglDonasi;
    }

    public void setTglDonasi(String tglDonasi) {
        this.tglDonasi = tglDonasi;
    }

    public String getKomentar() {
        return komentar;
    }

    public void setKomentar(String komentar) {
        this.komentar = komentar;
    }

}
