package com.example.ajiguna.milik_kita.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Aji Guna on 19/04/2018.
 */

public class Detail_Post_Story {
    @SerializedName("deskripsi_wisata")
    @Expose
    private String deskripsiWisata;

    public String getDeskripsiWisata() {
        return deskripsiWisata;
    }

    public void setDeskripsiWisata(String deskripsiWisata) {
        this.deskripsiWisata = deskripsiWisata;
    }
}
