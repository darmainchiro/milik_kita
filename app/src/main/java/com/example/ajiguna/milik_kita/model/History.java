package com.example.ajiguna.milik_kita.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Aji Guna on 20/04/2018.
 */

public class History {
    @SerializedName("nama_wisata")
    @Expose
    private String namaWisata;
    @SerializedName("jum_donasi")
    @Expose
    private String jumDonasi;
    @SerializedName("tgl_donasi")
    @Expose
    private String tglDonasi;

    public String getNamaWisata() {
        return namaWisata;
    }

    public void setNamaWisata(String namaWisata) {
        this.namaWisata = namaWisata;
    }

    public String getJumDonasi() {
        return jumDonasi;
    }

    public void setJumDonasi(String jumDonasi) {
        this.jumDonasi = jumDonasi;
    }

    public String getTglDonasi() {
        return tglDonasi;
    }

    public void setTglDonasi(String tglDonasi) {
        this.tglDonasi = tglDonasi;
    }
}
