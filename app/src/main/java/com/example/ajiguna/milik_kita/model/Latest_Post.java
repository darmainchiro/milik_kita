package com.example.ajiguna.milik_kita.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Aji Guna on 19/04/2018.
 */

public class Latest_Post {
    @SerializedName("id_wisata")
    @Expose
    private String idWisata;
    @SerializedName("nama_wisata")
    @Expose
    private String namaWisata;
    @SerializedName("user_name")
    @Expose
    private String userName;
    @SerializedName("donasi_butuh")
    @Expose
    private String donasiButuh;
    @SerializedName("lokasi")
    @Expose
    private String lokasi;
    @SerializedName("donasi_terkumpul")
    @Expose
    private String donasiTerkumpul;
    @SerializedName("gambar")
    @Expose
    private String gambar;
    @SerializedName("sisa_hari")
    @Expose
    private String sisaHari;

    public String getIdWisata() {
        return idWisata;
    }

    public void setIdWisata(String idWisata) {
        this.idWisata = idWisata;
    }

    public String getNamaWisata() {
        return namaWisata;
    }

    public void setNamaWisata(String namaWisata) {
        this.namaWisata = namaWisata;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getDonasiButuh() {
        return donasiButuh;
    }

    public void setDonasiButuh(String donasiButuh) {
        this.donasiButuh = donasiButuh;
    }

    public String getLokasi() {
        return lokasi;
    }

    public void setLokasi(String lokasi) {
        this.lokasi = lokasi;
    }

    public String getDonasiTerkumpul() {
        return donasiTerkumpul;
    }

    public void setDonasiTerkumpul(String donasiTerkumpul) {
        this.donasiTerkumpul = donasiTerkumpul;
    }

    public String getGambar() {
        return gambar;
    }

    public void setGambar(String gambar) {
        this.gambar = gambar;
    }

    public String getSisaHari() {
        return sisaHari;
    }

    public void setSisaHari(String sisaHari) {
        this.sisaHari = sisaHari;
    }
}
