package com.example.ajiguna.milik_kita.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Aji Guna on 20/04/2018.
 */

public class Profile {

    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("jum_donasi")
    @Expose
    private String jumDonasi;
    @SerializedName("jum_transaksi")
    @Expose
    private String jumTransaksi;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getJumDonasi() {
        return jumDonasi;
    }

    public void setJumDonasi(String jumDonasi) {
        this.jumDonasi = jumDonasi;
    }

    public String getJumTransaksi() {
        return jumTransaksi;
    }

    public void setJumTransaksi(String jumTransaksi) {
        this.jumTransaksi = jumTransaksi;
    }
}
