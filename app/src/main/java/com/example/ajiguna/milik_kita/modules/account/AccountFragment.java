package com.example.ajiguna.milik_kita.modules.account;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.ajiguna.milik_kita.R;
import com.example.ajiguna.milik_kita.api.ApiClient;
import com.example.ajiguna.milik_kita.api.ApiInterface;
import com.example.ajiguna.milik_kita.model.Profile;
import com.example.ajiguna.milik_kita.modules.galang.GalangActivity;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Aji Guna on 02/04/2018.
 */

public class AccountFragment extends Fragment {

    private TextView teks_syarat;
    private TextView teks_kebijakan;
    private TextView teks_user;
    private TextView teks_email;
    private TextView jumlah_donasi;
    private TextView jumlah_transaksi;
    private Button buat_btn;
    private Button lihat_btn;

    Call<List<Profile>> call;
    private ApiInterface apiInterface;

    public static AccountFragment newInstance() {
        AccountFragment fragment = new AccountFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_account,container, false);

        teks_user = (TextView)view.findViewById(R.id.user);
        teks_email = (TextView)view.findViewById(R.id.email);
        jumlah_donasi = (TextView)view.findViewById(R.id.jumlah_donasi);
        jumlah_transaksi = (TextView)view.findViewById(R.id.jumlah_transaksi);
        teks_syarat = (TextView)view.findViewById(R.id.teks_syarat);
        teks_kebijakan = (TextView)view.findViewById(R.id.teks_kebijakan);
        lihat_btn = (Button)view.findViewById(R.id.btn_lihat);
        buat_btn = (Button)view.findViewById(R.id.btn_buat);

        //Get Argument that passed from activity in "data" key value
        final String getArgument = getArguments().getString("data");
        //GET API
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        call = apiInterface.getProfile(getArgument);

        call.enqueue(new Callback<List<Profile>>() {
            @Override
            public void onResponse(Call<List<Profile>> call, final Response<List<Profile>> response) {

                try {
                    final List<Profile> myList = response.body();
                    for (Profile h : myList) {
                        teks_user.setText(h.getUsername());
                        teks_email.setText(h.getEmail());
                        jumlah_donasi.setText("Rp "+h.getJumDonasi());
                        jumlah_transaksi.setText(h.getJumTransaksi());
                    }
                    buat_btn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            for (Profile h : myList) {
                                Intent intent = new Intent(getActivity(), GalangActivity.class);
                                intent.putExtra("id_user", getArgument);
                                intent.putExtra("username", h.getUsername());
                                startActivity(intent);
                            }
                        }
                    });
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<List<Profile>> call_topics, Throwable t) {
                t.printStackTrace();
            }
        });

        lihat_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), GalangActivity.class);
                startActivity(intent);
            }
        });

        teks_syarat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), SyaratActivity.class);
                startActivity(intent);
            }
        });

        teks_kebijakan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), KebijakanActivity.class);
                startActivity(intent);
            }
        });
        return view;
    }
}
