package com.example.ajiguna.milik_kita.modules.account;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.ajiguna.milik_kita.R;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by Aji Guna on 04/04/2018.
 */

public class DonaturFragment extends Fragment {

    ArrayList personNames = new ArrayList<>(Arrays.asList("Yogi Putra", "Aji Guna", "Abdul Azis","Fajar Gilang", "Ahmad Ardi", "Lina Meritha", "Kila Kian", "Ibnu Raziq", "Wahyu Ade", "Afdolash"));
    ArrayList uang_donasi = new ArrayList<>(Arrays.asList("20.000", "100.000", "100.000","50.000", "50.000", "20.000", "75.000", "100.000", "50.000", "75.000"));
//    ArrayList personImages = new ArrayList<>(Arrays.asList(R.drawable.isembilan, R.drawable.icemenung, R.drawable.ivandenbosch, R.drawable.ikastoba, R.drawable.kelud, R.drawable.klotok, R.drawable.goasigologolo, R.drawable.ibanyumulok, R.drawable.ibudhatidur, R.drawable.ibukitkapursekapuk));

    public static DonaturFragment newInstance() {
        DonaturFragment fragment = new DonaturFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_donatur,container, false);

        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.recy_donatur);
        // set a LinearLayoutManager with default vertical orientation
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this.getActivity());
        recyclerView.setLayoutManager(linearLayoutManager);
        // call the constructor of DonasiAdapter to send the reference and data to Adapter
//        DonaturAdapter customAdapter = new DonaturAdapter(getActivity(), personNames,uang_donasi);
//        recyclerView.setAdapter(customAdapter);
        return view;
    }
}
