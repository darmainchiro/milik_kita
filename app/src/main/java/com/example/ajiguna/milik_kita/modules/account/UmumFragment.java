package com.example.ajiguna.milik_kita.modules.account;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.ajiguna.milik_kita.R;

/**
 * Created by Aji Guna on 04/04/2018.
 */

public class UmumFragment extends Fragment {

    private TextView teks_umum;

    public static UmumFragment newInstance() {
        UmumFragment fragment = new UmumFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_umum,container, false);

        teks_umum = (TextView)view.findViewById(R.id.teks_umum);

        teks_umum.setText(R.string.sembilan);

        return view;
    }
}
