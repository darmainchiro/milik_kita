package com.example.ajiguna.milik_kita.modules.article;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.ajiguna.milik_kita.R;
import com.example.ajiguna.milik_kita.api.ApiClient;
import com.example.ajiguna.milik_kita.api.ApiInterface;
import com.example.ajiguna.milik_kita.model.Detail_Post;
import com.example.ajiguna.milik_kita.modules.donasi.DonasiActivity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ArticleActivity extends AppCompatActivity {

    ArrayList personJudul = new ArrayList<>(Arrays.asList("Pantai Sembilan", "Bukit Cemenung", "Benteng Van den Bosch", "Danau Kastoba", "Kawah Gunung Kelud", "Gunung Klotok", "Goa Sigolo-Golo", "Tebing Banyu Mulok", "Budha Tidur", "Bukit Kapur Sekapuk"));
    ArrayList personImages = new ArrayList<>(Arrays.asList(R.drawable.isembilan, R.drawable.icemenung, R.drawable.ivandenbosch, R.drawable.ikastoba, R.drawable.kelud, R.drawable.klotok, R.drawable.goasigologolo, R.drawable.ibanyumulok, R.drawable.ibudhatidur, R.drawable.ibukitkapursekapuk));

    ArrayList personStory = new ArrayList<>(Arrays.asList(R.string.sembilan, R.string.cemenung, R.string.vanden, R.string.danau, R.string.kelud, R.string.klotok, R.string.goa, R.string.Mulok, R.string.tidur, R.string.bukit));

    Call <List<Detail_Post>> call;
    private ApiInterface apiInterface;



    private Toolbar toolbar;
    private TabLayout tabLayout;
    private FrameLayout frameLayout;
    private ViewPager viewPager;

    private ImageView cover_image;
    private ImageView avatar;
    private Button btn_donasi;

    private TextView teks_judul, nilai_dana, nilai_pencapaian, sisa_hari, username, date_donasi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_article);

        final String mPost_key = getIntent().getExtras().getString("id_wisata");
        final String id_user = getIntent().getExtras().getString("id_user");

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        cover_image = (ImageView) findViewById(R.id.coverImageView);
        avatar = (ImageView) findViewById(R.id.avatarImageView);

        teks_judul = (TextView)findViewById(R.id.titleTextView);
        nilai_dana = (TextView)findViewById(R.id.nilai_dana);
        nilai_pencapaian = (TextView)findViewById(R.id.nilai_pencapaian);
        sisa_hari = (TextView)findViewById(R.id.sisa_hari);

        username = (TextView)findViewById(R.id.authorNameTextView);
        date_donasi = (TextView)findViewById(R.id.dateTextView);
        btn_donasi = (Button)findViewById(R.id.btn_donasi);

        //GET API
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        call = apiInterface.getPostDetail(mPost_key);

        call.enqueue(new Callback<List<Detail_Post>>() {
            @Override
            public void onResponse(Call<List<Detail_Post>> call, Response<List<Detail_Post>> response) {

                try {
                    final List<Detail_Post> myList = response.body();
                    for (Detail_Post h : myList) {
                        Double nilai = Double.valueOf(h.getDonasiTerkumpul()).doubleValue() * 100 / Double.valueOf(h.getDonasiButuh()).doubleValue();
                        String pencapaian = String.format("%.2f", nilai);
                        Glide.with(getApplicationContext()).load(h.getGambar()).into(cover_image);
                        teks_judul.setText(h.getNamaWisata());
                        nilai_dana.setText("Rp "+ h.getDonasiButuh());
                        nilai_pencapaian.setText(pencapaian+"%");
                        sisa_hari.setText(h.getSisaHari());
                        username.setText(h.getUserName());
                        date_donasi.setText(h.getStartDate());
                    }

                    btn_donasi.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            for (Detail_Post h : myList) {
                                Intent i = new Intent(ArticleActivity.this, DonasiActivity.class);
                                i.putExtra("id_wisata", h.getIdWisata());
                                i.putExtra("nama_wisata", h.getNamaWisata());
                                i.putExtra("id_user", id_user);
                                i.putExtra("nama_user", h.getUserName());
                                startActivity(i);
                            }
                        }
                    });

                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<List<Detail_Post>> call_topics, Throwable t) {
                t.printStackTrace();
            }
        });


        frameLayout = (FrameLayout) findViewById(R.id.frame_layout);

        tabLayout = (TabLayout) findViewById(R.id.tabs);

        Fragment selectedFragment = new StoryFragment();
        Bundle data = new Bundle();//Use bundle to pass data
        data.putString("data", mPost_key);//put string, int, etc in bundle with a key value
        selectedFragment.setArguments(data);

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_layout, selectedFragment);
        transaction.commit();

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                Fragment selectedFragment = null;
                switch (tab.getPosition()) {
                    case 0:
                        selectedFragment = new StoryFragment();//Get Fragment Instance
                        Bundle data = new Bundle();//Use bundle to pass data
                        data.putString("data", mPost_key);//put string, int, etc in bundle with a key value
                        selectedFragment.setArguments(data);
                        break;
                    case 1:
                        selectedFragment = UpdateFragment.newInstance();
                        Bundle data2 = new Bundle();//Use bundle to pass data
                        data2.putString("data", mPost_key);//put string, int, etc in bundle with a key value
                        selectedFragment.setArguments(data2);
                        break;
                    case 2:
                        selectedFragment = new DonaturFragment();
                        Bundle data3 = new Bundle();//Use bundle to pass data
                        data3.putString("data", mPost_key);//put string, int, etc in bundle with a key value
                        selectedFragment.setArguments(data3);
                        break;
                }
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.frame_layout, selectedFragment);
                transaction.commit();
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

}
