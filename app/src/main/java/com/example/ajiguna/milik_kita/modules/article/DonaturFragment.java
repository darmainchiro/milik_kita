package com.example.ajiguna.milik_kita.modules.article;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.ajiguna.milik_kita.R;
import com.example.ajiguna.milik_kita.adapter.DonaturAdapter;
import com.example.ajiguna.milik_kita.api.ApiClient;
import com.example.ajiguna.milik_kita.api.ApiInterface;
import com.example.ajiguna.milik_kita.model.Detail_Post_Donatur;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Aji Guna on 04/04/2018.
 */

public class DonaturFragment extends Fragment {

    ArrayList personNames = new ArrayList<>(Arrays.asList("Yogi Putra", "Aji Guna", "Abdul Azis","Fajar Gilang", "Ahmad Ardi", "Lina Meritha", "Kila Kian", "Ibnu Raziq", "Wahyu Ade", "Afdolash"));
    ArrayList uang_donasi = new ArrayList<>(Arrays.asList("20.000", "100.000", "100.000","50.000", "50.000", "20.000", "75.000", "100.000", "50.000", "75.000"));
//    ArrayList personImages = new ArrayList<>(Arrays.asList(R.drawable.isembilan, R.drawable.icemenung, R.drawable.ivandenbosch, R.drawable.ikastoba, R.drawable.kelud, R.drawable.klotok, R.drawable.goasigologolo, R.drawable.ibanyumulok, R.drawable.ibudhatidur, R.drawable.ibukitkapursekapuk));

    private List<Detail_Post_Donatur> postku;
    Call<List<Detail_Post_Donatur>> call;
    private ApiInterface apiInterface;
    private DonaturAdapter adapter;
    private RecyclerView recyclerView;

    public DonaturFragment(){
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_donatur,container, false);

        //Get Argument that passed from activity in "data" key value
        String getArgument = getArguments().getString("data");
        //GET API
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        call = apiInterface.getPostDetailDonatur(getArgument);
        postku = new ArrayList<>();


        recyclerView = (RecyclerView) view.findViewById(R.id.recy_donatur);
        // set a LinearLayoutManager with default vertical orientation
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this.getActivity());
        recyclerView.setLayoutManager(linearLayoutManager);
        // call the constructor of DonasiAdapter to send the reference and data to Adapter

        GetData();


        return view;
    }

    private void GetData() {

        call.enqueue(new Callback<List<Detail_Post_Donatur>>() {
            @Override
            public void onResponse(Call<List<Detail_Post_Donatur>> call, Response<List<Detail_Post_Donatur>> response) {

                postku = response.body();
                adapter = new DonaturAdapter(getActivity(), postku);
                recyclerView.setAdapter(adapter);
            }

            @Override
            public void onFailure(Call<List<Detail_Post_Donatur>> call_topics, Throwable t) {
                t.printStackTrace();
            }
        });
    }
}
