package com.example.ajiguna.milik_kita.modules.article;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.ajiguna.milik_kita.R;
import com.example.ajiguna.milik_kita.api.ApiClient;
import com.example.ajiguna.milik_kita.api.ApiInterface;
import com.example.ajiguna.milik_kita.model.Detail_Post_Story;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Aji Guna on 04/04/2018.
 */

public class StoryFragment extends Fragment {

    private TextView teks_story;
    Call<List<Detail_Post_Story>> call;
    private ApiInterface apiInterface;

    public StoryFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_story,container, false);

//        String mPost_key = getActivity().getIntent().getExtras().getString("id_wisata");

        teks_story = (TextView)view.findViewById(R.id.teks_story);

        //Get Argument that passed from activity in "data" key value
        String getArgument = getArguments().getString("data");
        //GET API
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        call = apiInterface.getPostDetailStory(getArgument);

        call.enqueue(new Callback<List<Detail_Post_Story>>() {
            @Override
            public void onResponse(Call<List<Detail_Post_Story>> call, Response<List<Detail_Post_Story>> response) {

                try {
                    final List<Detail_Post_Story> myList = response.body();
                    for (Detail_Post_Story h : myList) {
                        teks_story.setText(h.getDeskripsiWisata());
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<List<Detail_Post_Story>> call_topics, Throwable t) {
                t.printStackTrace();
            }
        });

//        teks_story.setText(R.string.sembilan);

        return view;
    }
}
