package com.example.ajiguna.milik_kita.modules.article;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.ajiguna.milik_kita.R;

/**
 * Created by Aji Guna on 04/04/2018.
 */

public class UpdateFragment extends Fragment {

    public static UpdateFragment newInstance() {
        UpdateFragment fragment = new UpdateFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_visit_history,container, false);
        return view;
    }
}
