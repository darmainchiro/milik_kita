package com.example.ajiguna.milik_kita.modules.donasi;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

import com.example.ajiguna.milik_kita.MainActivity;
import com.example.ajiguna.milik_kita.R;
import com.example.ajiguna.milik_kita.api.ApiClient;
import com.example.ajiguna.milik_kita.api.ApiInterface;
import com.example.ajiguna.milik_kita.model.Donasi;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Aji Guna on 05/04/2018.
 */

public class DonasiActivity extends AppCompatActivity{

    private Button btn_donasi;
    Dialog MyDialog;
    String id_wisata;
    String nama_wisata;
    String id_user;
    String nama_user;
    String str;

    ApiInterface apiInterface;
    Call<Donasi> call;

    private EditText nilai_nominal,isi_komentar;

    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_donasi);

        id_wisata = getIntent().getExtras().getString("id_wisata");
        nama_wisata = getIntent().getExtras().getString("nama_wisata");
        id_user = getIntent().getExtras().getString("id_user");
        nama_user = getIntent().getExtras().getString("nama_user");


        btn_donasi = (Button)findViewById(R.id.btn_donasi);
        nilai_nominal = (EditText)findViewById(R.id.nilai_nominal);
        isi_komentar = (EditText)findViewById(R.id.isi_komentar);

        progressDialog = new ProgressDialog(this);

        btn_donasi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                send_donasi();
            }
        });

    }

    public void onRadioButtonClicked(View view) {
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();
        // Check which radio button was clicked
        switch(view.getId()) {
            case R.id.radioButton1:
                if (checked)
                    str = "BNI";
                break;
            case R.id.radioButton2:
                if (checked)
                    str = "BCA";
                    break;
            case R.id.radioButton3:
                if (checked)
                    str = "BRI";
                    break;
            case R.id.radioButton4:
                if (checked)
                    str = "Mandiri";
                break;
            case R.id.radioButton5:
                if (checked)
                    str = "Bank Danamon";
                break;
        }
    }

    private void send_donasi() {
        final String nominal = nilai_nominal.getText().toString().trim();
        String komentar = isi_komentar.getText().toString().trim();
        final String pembayaran = isi_komentar.getText().toString().trim();
        HashMap<String, String> params = new HashMap<>();
        params.put("id_wisata", id_wisata);
        params.put("nama_wisata", nama_wisata);
        params.put("id_user", id_user);
        params.put("username", nama_user);
        params.put("jum_donasi", nominal);
        params.put("pembayaran", str);
        params.put("komentar", komentar);
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        call = apiInterface.getDonasi(params);

        progressDialog.setMessage("Donasi...");
        progressDialog.show();
        progressDialog.setCanceledOnTouchOutside(false);
        call.enqueue(new Callback<Donasi>() {
            @Override
            public void onResponse(Call<Donasi> call, Response<Donasi> response) {
                progressDialog.dismiss();
                Intent mainIntent = new Intent(DonasiActivity.this, DonasiSelesai.class);
                mainIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                mainIntent.putExtra("nominal", nominal);
                mainIntent.putExtra("bank", pembayaran);
                startActivity(mainIntent);
            }
            @Override
            public void onFailure(Call<Donasi> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(DonasiActivity.this,"Donasi Error",Toast.LENGTH_LONG).show();
            }
        });
    }

    public void MyCustomlertDialog(){
        MyDialog = new Dialog(this);
        MyDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        MyDialog.setContentView(R.layout.customdialog_galang);

        Button masuk = (Button) MyDialog.findViewById(R.id.btn_ok);

        masuk.setEnabled(true);
        masuk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                MyDialog.cancel();
                Intent intent = new Intent(DonasiActivity.this, MainActivity.class);
                startActivity(intent);
            }
        });
        MyDialog.show();
    }
}
