package com.example.ajiguna.milik_kita.modules.donasi;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.ajiguna.milik_kita.R;

/**
 * Created by Aji Guna on 20/04/2018.
 */

public class DonasiSelesai extends AppCompatActivity {

    private TextView nominal;
    private ImageView img_bank;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_selesai_donasi);

        final String bayar = getIntent().getExtras().getString("nominal");
        final String bank = getIntent().getExtras().getString("bank");

        nominal = (TextView)findViewById(R.id.teks_bayar);
        img_bank = (ImageView)findViewById(R.id.img_bank);

        nominal.setText(bayar);
        if (bank.equals("BNI")){
            img_bank.setImageDrawable(getResources().getDrawable(R.drawable.bank_bni));
        }else if (bank.equals("BCA")){
            img_bank.setImageDrawable(getResources().getDrawable(R.drawable.bank_bca));
        }else if (bank.equals("BRI")){
            img_bank.setImageDrawable(getResources().getDrawable(R.drawable.bank_bri));
        }else if (bank.equals("Mandiri")){
            img_bank.setImageDrawable(getResources().getDrawable(R.drawable.bank_mandiri));
        }else if (bank.equals("Bank Danamon")){
            img_bank.setImageDrawable(getResources().getDrawable(R.drawable.bank_danamon));
        }
    }
}
