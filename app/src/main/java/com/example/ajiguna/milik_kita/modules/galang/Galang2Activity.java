package com.example.ajiguna.milik_kita.modules.galang;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;

import com.example.ajiguna.milik_kita.R;

/**
 * Created by Aji Guna on 20/04/2018.
 */

public class Galang2Activity extends AppCompatActivity{

    private Toolbar toolbar;
    private Button btn_selesai;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_galang2);

        toolbar = (Toolbar)findViewById(R.id.profileToolBar);
        toolbar.setTitle("Upload Identitas Diri");

        btn_selesai = (Button)findViewById(R.id.btn_selesai);

        btn_selesai.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Galang2Activity.this, GalangSelesai.class);
                startActivity(intent);
            }
        });
    }
}
