package com.example.ajiguna.milik_kita.modules.galang;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.util.Base64;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;
import android.widget.Toolbar;

import com.example.ajiguna.milik_kita.R;
import com.example.ajiguna.milik_kita.api.ApiClient;
import com.example.ajiguna.milik_kita.api.ApiInterface;
import com.example.ajiguna.milik_kita.model.Galang;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Aji Guna on 09/04/2018.
 */

public class GalangActivity extends AppCompatActivity{

    private static final int GALLERY_REQUEST = 1;
    private Uri mImageUri = null;
    private Button btn_galang;
    private Toolbar toolbar;
    Dialog MyDialog;
    private ImageButton image_select;
    private EditText teks_judul;
    private EditText teks_target;
    private EditText teks_deksripsi;
    private EditText teks_lokasi;
    private EditText teks_deadline;

    private DatePickerDialog endDatePickerDialog;

    private SimpleDateFormat dateFormatter;

    String encode_image;
    String username;
    String id_user;

    ApiInterface apiInterface;
    Call<Galang> call;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_galang);

//        toolbar = (Toolbar)findViewById(R.id.profileToolBar);
//        toolbar.setTitle("Penggalangan Dana");
        username = getIntent().getExtras().getString("username");
        id_user = getIntent().getExtras().getString("id_user");

        teks_judul = (EditText)findViewById(R.id.teks_judul);
        teks_deadline = (EditText)findViewById(R.id.teks_deadline);
        teks_deksripsi = (EditText)findViewById(R.id.teks_deks);
        teks_lokasi = (EditText)findViewById(R.id.teks_lokasi);
        teks_target = (EditText)findViewById(R.id.teks_target);

        progressDialog = new ProgressDialog(this);

        dateFormatter = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        teks_deadline.setInputType(InputType.TYPE_NULL);
        teks_deadline.setKeyListener(null);
        teks_deadline.requestFocus();

        setDateTimeField();

        btn_galang = (Button)findViewById(R.id.btn_kirim);
        image_select = (ImageButton)findViewById(R.id.image_select);
        image_select.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent galleryIntent = new Intent(Intent.ACTION_GET_CONTENT);
                galleryIntent.setType("image/*");
                startActivityForResult(galleryIntent, GALLERY_REQUEST);
            }
        });

        btn_galang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendGalang();
//                MyCustomlertDialog();
//                Toast.makeText(GalangActivity.this, encode_image, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void setDateTimeField() {
        teks_deadline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(view == teks_deadline) {
                    endDatePickerDialog.show();
                }
            }
        });

        Calendar newCalendar = Calendar.getInstance();
        endDatePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

            @RequiresApi(api = Build.VERSION_CODES.N)
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                teks_deadline.setText(dateFormatter.format(newDate.getTime()));
            }

        },newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));

    }


    private void sendGalang() {
        String judul = teks_judul.getText().toString().trim();
        String target = teks_target.getText().toString().trim();
        String deskripsi = teks_deksripsi.getText().toString().trim();
        String lokasi = teks_lokasi.getText().toString().trim();
        String deadline = teks_deadline.getText().toString().trim();

        HashMap<String, String> params = new HashMap<>();
        params.put("nama_wisata", judul);
        params.put("donasi_butuh", target);
        params.put("username", username);
        params.put("end_date", deadline);
        params.put("lokasi", lokasi);
        params.put("deskripsi", deskripsi);
        params.put("image_name", encode_image);
        params.put("user_id", id_user);

        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        call = apiInterface.getGalang(params);

        progressDialog.setMessage("Membuat Campaign...");
        progressDialog.show();
        progressDialog.setCanceledOnTouchOutside(false);
        call.enqueue(new Callback<Galang>() {
            @Override
            public void onResponse(Call<Galang> call, Response<Galang> response) {
                progressDialog.dismiss();
                Intent mainIntent = new Intent(GalangActivity.this, Galang2Activity.class);
                mainIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                mainIntent.putExtra("id_user",id_user);
                mainIntent.putExtra("username", username);
                startActivity(mainIntent);
            }
            @Override
            public void onFailure(Call<Galang> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(GalangActivity.this,"Campaign Error",Toast.LENGTH_LONG).show();
            }
        });


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == GALLERY_REQUEST && resultCode == RESULT_OK){
            mImageUri = data.getData();
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), mImageUri);
                image_select.setImageBitmap(bitmap);
                encode_image = encodeTobase64(bitmap);
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }

    public void MyCustomlertDialog(){
        MyDialog = new Dialog(this);
        MyDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        MyDialog.setContentView(R.layout.customdialog_galang);

        Button masuk = (Button) MyDialog.findViewById(R.id.btn_ok);

        masuk.setEnabled(true);
        masuk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyDialog.cancel();
            }
        });
        MyDialog.show();
    }

    public static String encodeTobase64(Bitmap image)
    {
        String imageEncoded;
        Bitmap imagex=image;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        imagex.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        Bitmap.createScaledBitmap(imagex,250 , 600, false);
        byte[] byte_arr = baos.toByteArray();
        // Encode Image to String
        imageEncoded = Base64.encodeToString(byte_arr, 0);

        return imageEncoded;
    }
}
