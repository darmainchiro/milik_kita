package com.example.ajiguna.milik_kita.modules.galang;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.example.ajiguna.milik_kita.R;

/**
 * Created by Aji Guna on 20/04/2018.
 */

public class GalangSelesai extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_selesai_galang);
    }
}
