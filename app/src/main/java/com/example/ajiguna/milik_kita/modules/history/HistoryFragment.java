package com.example.ajiguna.milik_kita.modules.history;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.ajiguna.milik_kita.R;
import com.example.ajiguna.milik_kita.adapter.HistoryAdapter;
import com.example.ajiguna.milik_kita.api.ApiClient;
import com.example.ajiguna.milik_kita.api.ApiInterface;
import com.example.ajiguna.milik_kita.model.History;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Aji Guna on 02/04/2018.
 */

public class HistoryFragment extends Fragment {

    private TextView teks_donasi;
    private RecyclerView recyclerView;

    Call<List<History>> call;
    private List<History> postku;
    private ApiInterface apiInterface;
    private HistoryAdapter adapter;

    String getArgument;

    public static HistoryFragment newInstance() {
        HistoryFragment fragment = new HistoryFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_visit_history,container, false);

//        teks_donasi = (TextView)view.findViewById(R.id.history_jumlah_donasi);
        recyclerView = (RecyclerView)view.findViewById(R.id.recy_history);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this.getActivity());
        recyclerView.setLayoutManager(linearLayoutManager);

        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        postku = new ArrayList<>();

        getArgument = getArguments().getString("data");

        GetData();


        return view;
    }

    private void GetData() {
        //Get Argument that passed from activity in "data" key value


        call = apiInterface.getHistory(getArgument);
        call.enqueue(new Callback<List<History>>() {
            @Override
            public void onResponse(Call<List<History>> call_topics, Response<List<History>> response) {

                postku = response.body();
                adapter = new HistoryAdapter(getActivity(), postku);
                recyclerView.setAdapter(adapter);
            }

            @Override
            public void onFailure(Call<List<History>> call_topics, Throwable t) {
                t.printStackTrace();
            }

        });
    }
}
