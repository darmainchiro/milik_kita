package com.example.ajiguna.milik_kita.modules.home;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.ajiguna.milik_kita.R;
import com.example.ajiguna.milik_kita.adapter.DonasiAdapter;
import com.example.ajiguna.milik_kita.api.ApiClient;
import com.example.ajiguna.milik_kita.api.ApiInterface;
import com.example.ajiguna.milik_kita.model.Latest_Post;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Aji Guna on 02/04/2018.
 */

public class HomeFragment extends Fragment {

    ArrayList personNames = new ArrayList<>(Arrays.asList("Pantai Sembilan", "Bukit Cemenung", "Benteng Van den Bosch", "Danau Kastoba", "Kawah Gunung Kelud", "Gunung Klotok", "Goa Sigolo-Golo", "Tebing Banyu Mulok", "Budha Tidur", "Bukit Kapur Sekapuk"));
    ArrayList personImages = new ArrayList<>(Arrays.asList(R.drawable.isembilan, R.drawable.icemenung, R.drawable.ivandenbosch, R.drawable.ikastoba, R.drawable.kelud, R.drawable.klotok, R.drawable.goasigologolo, R.drawable.ibanyumulok, R.drawable.ibudhatidur, R.drawable.ibukitkapursekapuk));

    Call<List<Latest_Post>> call;
    private List<Latest_Post> postku;
    private ApiInterface apiInterface;
    private DonasiAdapter adapter;
    private RecyclerView recyclerView;

    String getArgument;

    public static HomeFragment newInstance() {
        HomeFragment fragment = new HomeFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home,container, false);

        getArgument = getArguments().getString("data");

        recyclerView = (RecyclerView) view.findViewById(R.id.recy_home);
        // set a LinearLayoutManager with default vertical orientation
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this.getActivity());
        recyclerView.setLayoutManager(linearLayoutManager);
        // call the constructor of DonasiAdapter to send the reference and data to Adapter
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        postku = new ArrayList<>();
        GetData();
        return view;
    }

    private void GetData() {
        call = apiInterface.getPost();
        call.enqueue(new Callback<List<Latest_Post>>() {
            @Override
            public void onResponse(Call<List<Latest_Post>> call_topics, Response<List<Latest_Post>> response) {

                postku = response.body();
                adapter = new DonasiAdapter(getActivity(), postku, getArgument);
                recyclerView.setAdapter(adapter);
            }

            @Override
            public void onFailure(Call<List<Latest_Post>> call_topics, Throwable t) {
                t.printStackTrace();
            }

        });
    }
}
