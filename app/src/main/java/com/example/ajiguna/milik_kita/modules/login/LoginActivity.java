package com.example.ajiguna.milik_kita.modules.login;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ajiguna.milik_kita.MainActivity;
import com.example.ajiguna.milik_kita.R;
import com.example.ajiguna.milik_kita.api.ApiClient;
import com.example.ajiguna.milik_kita.api.ApiInterface;
import com.example.ajiguna.milik_kita.model.Login;
import com.example.ajiguna.milik_kita.modules.register.RegisterActivity;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Aji Guna on 09/04/2018.
 */

public class LoginActivity extends AppCompatActivity {

    private Button btn_masuk;
    private TextView teks_daftar;
    private EditText teks_username, teks_password;

    private ProgressDialog progressDialog;

    ApiInterface apiInterface;
    Call<Login> call;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        teks_username = (EditText)findViewById(R.id.teks_username);
        teks_password = (EditText)findViewById(R.id.teks_password);
        btn_masuk = (Button)findViewById(R.id.btn_masuk);
        teks_daftar = (TextView)findViewById(R.id.teks_daftar);

        progressDialog = new ProgressDialog(this);

        btn_masuk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                send_login();
            }
        });

        teks_daftar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
                startActivity(intent);
            }
        });
    }

    private void send_login() {
        String username = teks_username.getText().toString().trim();
        String password = teks_password.getText().toString().trim();
        HashMap<String, String> params = new HashMap<>();
        params.put("username", username);
        params.put("password", password);
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        call = apiInterface.getLogin(params);

        progressDialog.setMessage("Masuk...");
        progressDialog.show();
        progressDialog.setCanceledOnTouchOutside(false);
        call.enqueue(new Callback<Login>() {
            @Override
            public void onResponse(Call<Login> call, Response<Login> response) {
                progressDialog.dismiss();
                Intent mainIntent = new Intent(LoginActivity.this, MainActivity.class);
                mainIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                mainIntent.putExtra("id_user", response.body().getData().getUserId());
                startActivity(mainIntent);
            }
            @Override
            public void onFailure(Call<Login> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(LoginActivity.this,"Login Error",Toast.LENGTH_LONG).show();
            }
        });
    }
}
