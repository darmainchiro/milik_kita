package com.example.ajiguna.milik_kita.modules.register;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ajiguna.milik_kita.MainActivity;
import com.example.ajiguna.milik_kita.R;
import com.example.ajiguna.milik_kita.api.ApiClient;
import com.example.ajiguna.milik_kita.api.ApiInterface;
import com.example.ajiguna.milik_kita.model.Register;
import com.example.ajiguna.milik_kita.modules.galang.GalangActivity;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Aji Guna on 09/04/2018.
 */

public class RegisterActivity extends AppCompatActivity {

    private Button btn_daftar;
    private TextView teks_masuk;
    private EditText teks_username, teks_email, teks_password;
    private ProgressDialog progressDialog;

    ApiInterface apiInterface;
    Call<Register> call;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        teks_username = (EditText)findViewById(R.id.regist_teks_username);
        teks_email = (EditText)findViewById(R.id.regist_teks_email);
        teks_password = (EditText)findViewById(R.id.regist_teks_password);
        btn_daftar = (Button)findViewById(R.id.btn_regist);
        teks_masuk = (TextView)findViewById(R.id.teks_masuk);

        progressDialog = new ProgressDialog(this);

        btn_daftar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                send_register();
            }
        });

        teks_masuk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(RegisterActivity.this, GalangActivity.class);
                startActivity(intent);
            }
        });
    }

    private void send_register() {
        String username = teks_username.getText().toString().trim();
        String email = teks_email.getText().toString().trim();
        String password = teks_password.getText().toString().trim();
        HashMap<String, String> params = new HashMap<>();
        params.put("username", username);
        params.put("email", email);
        params.put("password", password);
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        call = apiInterface.getRegister(params);
        progressDialog.setMessage("Mendaftar...");
        progressDialog.show();
        progressDialog.setCanceledOnTouchOutside(false);

        call.enqueue(new Callback<Register>() {
            @Override
            public void onResponse(Call<Register> call, Response<Register> response) {
                progressDialog.dismiss();
                Intent mainIntent = new Intent(RegisterActivity.this, MainActivity.class);
                mainIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                mainIntent.putExtra("id_user", response.body().getResult().getIdUser());
                startActivity(mainIntent);
            }

            @Override
            public void onFailure(Call<Register> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(RegisterActivity.this,"Register Error",Toast.LENGTH_LONG).show();
            }
        });
    }
}
